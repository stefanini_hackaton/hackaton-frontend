import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientRegisterComponent } from 'app/components/client-register.component'
import { ClientDashboardComponent } from 'app/components/client-dashboard.component'
import { ClientService } from 'app/services/client.service'

@NgModule({
  declarations: [
    AppComponent,
    ClientDashboardComponent,
    ClientRegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
