# HackatonFrontEnd

Este projeto visa disponibilizar a parte de Front-End para a aplicação de registro de cliente para utilização no Hackaton

## Development server

Execute o comando `ng serve` para iniciar o servidor em modo `desenvolvimento`. Acesse a página `http://localhost:4200/` para visualizar a aplicação.
As alterações realizadas na aplicação são automaticamente carregadas.

## Build

Execute o comando `ng build` para construir (build) do proeto. Os artefatos de construção estarão na pasta `dist/`.
