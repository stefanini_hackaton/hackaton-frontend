import {Component, OnInit} from '@angular/core';
import {ClientService} from 'app/services/client.service';
import { Client } from './client';

@Component({
    selector: 'app-client-dashboard',
    templateUrl: './client-dashboard.component.html',
    styleUrls: ['./client-dashboard.component.css']
  })

  export class ClientDashboardComponent implements OnInit {

    clients: Client[] = [];

    constructor(
      private clientService: ClientService
    ) {}

    ngOnInit(): void {
      this.clientService.getClients().then(clients => this.clients = clients);
    }

  }
